package com.dnulabs.check.health.constants;

public enum Constants {

	CONTEXTCONFIG("abilityConfig"),
	STRATEGIES("strategies"),
	CONNECTION("connection"),
	DRIVER("driver"),
	USERNAME("username"),
	PASSWORD("password"),
	PACKAGES("packages"),
	PACKAGE("package"),
	CATEGORIES("categories"),
	PROPERTY("property");
	
	private String value;
	
	private Constants(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public static final String LITERAL_DRIVER 						= "driver";
	public static final String LITERAL_USERNAME		 				= "username";
	public static final String LITERAL_PASSWORD 					= "password";
	public static final String LITERAL_URL 							= "url";
	
	public static final String TABLE_APPLICATION					= "application";
	public static final String TABLE_APPENV 						= "app_env";
	public static final String TABLE_ENV_HEALTH_STATUS 				= "env_health_status";
	
	public static final String COLUMN_APP_ID 						= "app_id";
	public static final String COLUMN_APP_NAME 						= "app_name";
	public static final String COLUMN_ENV_ID 						= "env_id";
	public static final String COLUMN_STATUS 						= "status";
	public static final String COLUMN_MESSAGE 						= "message";
	public static final String COLUMN_EXCEPION 						= "exception";
	public static final String COLUMN_LAST_UPDATED_DATE				= "lst_upd_dt";
	public static final String COLUMN_LAST_UPDATED_TIMESTAMP		= "lst_upd_ts";
	
	public static final String APP_LEVEL	 						= "level";
	
	public static final String DIALECT_HBM_MYSQL 					= "org.hibernate.dialect.MySQLDialect";
	
}
