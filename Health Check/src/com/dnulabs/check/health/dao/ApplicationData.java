package com.dnulabs.check.health.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dnulabs.check.health.constants.Constants;

@Entity
@Table(name =Constants.TABLE_APPLICATION)
public class ApplicationData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2831592558645383970L;

	@Id
	@Column(name=Constants.COLUMN_APP_ID)
	private String appId;
	
	@Column(name=Constants.COLUMN_APP_NAME, nullable=false)
	private String appName;

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the appName
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * @param appName the appName to set
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
}