package com.dnulabs.check.health.dao;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.dnulabs.check.health.constants.Constants;

@Entity
@Table(name =Constants.TABLE_APPENV)
public class ApplicationEnv implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6320787516252046502L;


	@EmbeddedId
	private ApplicationEnvKey envKey;

	/**
	 * @return the envKey
	 */
	public ApplicationEnvKey getEnvKey() {
		return envKey;
	}

	/**
	 * @param envKey the envKey to set
	 */
	public void setEnvKey(ApplicationEnvKey envKey) {
		this.envKey = envKey;
	}
}