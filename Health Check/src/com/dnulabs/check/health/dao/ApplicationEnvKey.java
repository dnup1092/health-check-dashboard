package com.dnulabs.check.health.dao;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.dnulabs.check.health.constants.Constants;

@Embeddable
public class ApplicationEnvKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6949436869035683601L;

	@Column(name = Constants.COLUMN_ENV_ID)
	//@Id
	private String envId;

	@ManyToOne(cascade = { CascadeType.REMOVE })
	@JoinColumns(value={@JoinColumn(name = "app_id", referencedColumnName = "app_id")})
	@NotNull
	private ApplicationData appId;

	/**
	 * @return the envId
	 */
	public String getEnvId() {
		return envId;
	}

	/**
	 * @param envId
	 *            the envId to set
	 */
	public void setEnvId(String envId) {
		this.envId = envId;
	}

	/**
	 * @return the appId
	 */
	public ApplicationData getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(ApplicationData appId) {
		this.appId = appId;
	}

}
