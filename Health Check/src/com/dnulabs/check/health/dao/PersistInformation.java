package com.dnulabs.check.health.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.dnulabs.check.health.utils.DBUtility;

public class PersistInformation {

	public boolean saveInformation(PersistenceDataObject data) {
		SessionFactory sessionFactory = DBUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction(); 
		try {
		session.save(data);
		transaction.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
