package com.dnulabs.check.health.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.dnulabs.check.health.constants.Constants;

@Entity
@Table(name = Constants.TABLE_ENV_HEALTH_STATUS)
public final class PersistenceDataObject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4670543545214135976L;

	@EmbeddedId
	private PersistenceDataObjectKey dataKey;
	
	@Column(name=Constants.COLUMN_STATUS)
	private String status;
	
	@Column(name=Constants.COLUMN_MESSAGE)
	private String message;
	
	@Column(name=Constants.COLUMN_EXCEPION)
	private String exception;
	
	@Column(name=Constants.COLUMN_LAST_UPDATED_DATE)
	@Temporal(TemporalType.DATE)
	private Date lastUpdatedDate;
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the exception
	 */
	public String getException() {
		return exception;
	}
	/**
	 * @param exception the exception to set
	 */
	public void setException(String exception) {
		this.exception = exception;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the dataKey
	 */
	public PersistenceDataObjectKey getDataKey() {
		return dataKey;
	}
	/**
	 * @param dataKey the dataKey to set
	 */
	public void setDataKey(PersistenceDataObjectKey dataKey) {
		this.dataKey = dataKey;
	}
}
