package com.dnulabs.check.health.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.dnulabs.check.health.constants.Constants;

@Embeddable
public class PersistenceDataObjectKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6949436869035683601L;

	@OneToOne(cascade={CascadeType.ALL})
	private ApplicationEnv envId;

	@Column(name=Constants.COLUMN_LAST_UPDATED_TIMESTAMP)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date lastUpdatedTimestamp;

	/**
	 * @return the envId
	 */
	public ApplicationEnv getEnvId() {
		return envId;
	}

	/**
	 * @param envId the envId to set
	 */
	public void setEnvId(ApplicationEnv envId) {
		this.envId = envId;
	}

	/**
	 * @return the lastUpdatedTimestamp
	 */
	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	/**
	 * @param lastUpdatedTimestamp the lastUpdatedTimestamp to set
	 */
	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}
}
