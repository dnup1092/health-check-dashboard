package com.dnulabs.check.health.event.listener;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.dnulabs.check.health.annotations.Ability;
import com.dnulabs.check.health.constants.Constants;
import com.dnulabs.check.health.exceptions.GenericException;
import com.dnulabs.check.health.exceptions.NoAblilityConfigException;
import com.dnulabs.check.health.utils.ClassScanner;
import com.dnulabs.check.health.utils.DBUtility;

@WebListener
public class AbilityContextListner implements ServletContextListener {

	
	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 4198953301865193126L;

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		//DBUtility.shutdown();		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		// TODO Auto-generated method stub
		ServletContext context = event.getServletContext();
				String configFile = context.getInitParameter(Constants.CONTEXTCONFIG.toString());
		
		if(configFile == null || configFile.isEmpty()) {
			throw new NoAblilityConfigException("Failed to load ability configuration. Please check your application classpath");
		}
		
		ArrayList<String> packages = new ArrayList<String>();
		HashMap<String,String> connectionParameter = new HashMap<String,String>();
		//XML Parsing starts ----- put this in separate class
		DocumentBuilderFactory xmlFactory = null;
		DocumentBuilder xmlParseBuilder = null;
		Document xmlStructure = null;
		NodeList packageNodes = null;
		NodeList connectionNodes = null;
		ArrayList<Object> classes = null;
		HashMap<String,Object> strategies = new HashMap<String,Object>();
		HashMap<String,List<String>> categories = new HashMap<String,List<String>>();
		try {
			xmlFactory = DocumentBuilderFactory.newInstance();
			xmlParseBuilder = xmlFactory.newDocumentBuilder();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			URL configUrl = loader.getResource(configFile);
			xmlStructure = xmlParseBuilder.parse(new File(configUrl.getPath()));
			packageNodes = xmlStructure.getElementsByTagName(Constants.PACKAGE.toString());
			connectionNodes = xmlStructure.getElementsByTagName(Constants.PROPERTY.toString());
			
			if(packageNodes.getLength() <= 0 ||  connectionNodes.getLength() <= 0) {
				throw new GenericException("Not able to detect");
			}
			for(int walker=0; walker<packageNodes.getLength(); walker++) {
				packages.add(packageNodes.item(walker).getFirstChild().getNodeValue().trim());
			}
			System.out.println("Packages: " + packages);
			for(int walker=0; walker<connectionNodes.getLength(); walker++) {
				connectionParameter.put(connectionNodes.item(walker).getAttributes().item(0).getNodeValue().trim(), connectionNodes.item(walker).getFirstChild().getNodeValue().trim());
			}
         
			for(String pkg : packages) {
				try{
					classes = ClassScanner.getClasses(pkg);
					for(Object cls :classes){
						Class<?> objClass = loader.loadClass(cls.toString());
						Annotation annotation = objClass.getAnnotation(Ability.class);
						if(annotation != null) {
							Ability ability = (Ability) annotation;
							strategies.put(ability.name(),objClass.newInstance());
							if(categories.containsKey(ability.category().toUpperCase())) {
								List<String> list = categories.get(ability.category().toUpperCase());
								list.add(ability.name());
							} else {
								List<String> list = new ArrayList<String>();
								list.add(ability.name());
								categories.put(ability.category().toUpperCase(),list);
							}
						}
					}
					
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException e) {
					throw new GenericException("Not able instantiate classes inside the package " + pkg + " . " + e.getMessage(), e);
				}
			}
			
			String url = connectionParameter.get(Constants.LITERAL_URL);
			String username = connectionParameter.get(Constants.LITERAL_USERNAME);
			String password = connectionParameter.get(Constants.LITERAL_PASSWORD);
			String driver = connectionParameter.get(Constants.LITERAL_DRIVER);
			String dialect = Constants.DIALECT_HBM_MYSQL; 
			context.setAttribute(Constants.STRATEGIES.toString(), strategies);
			context.setAttribute(Constants.CONNECTION.toString(), connectionParameter);
			context.setAttribute(Constants.CATEGORIES.toString(), categories);		
			DBUtility.initializeSessionFactory(driver, url, username, password, dialect);
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new NoAblilityConfigException("Not able to parse Ability configuration. " + e.getMessage(),e);
		} 
	}
}
