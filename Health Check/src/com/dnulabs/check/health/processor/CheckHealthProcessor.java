package com.dnulabs.check.health.processor;

import com.dnulabs.check.health.dao.ApplicationData;
import com.dnulabs.check.health.dao.ApplicationEnv;
import com.dnulabs.check.health.dao.PersistInformation;
import com.dnulabs.check.health.dao.PersistenceDataObject;

public abstract class CheckHealthProcessor {

	public abstract PersistenceDataObject requestProcessor(ApplicationData appInfo, ApplicationEnv appEnv);
	
	public void tearDown(PersistenceDataObject data ) {
		PersistInformation info = new PersistInformation();
		info.saveInformation(data); 
	}
	
	
}
