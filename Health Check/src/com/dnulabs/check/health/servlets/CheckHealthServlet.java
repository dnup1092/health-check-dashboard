package com.dnulabs.check.health.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.dnulabs.check.health.constants.Constants;
import com.dnulabs.check.health.dao.ApplicationData;
import com.dnulabs.check.health.dao.ApplicationEnv;
import com.dnulabs.check.health.dao.ApplicationEnvKey;
import com.dnulabs.check.health.dao.PersistenceDataObject;
import com.dnulabs.check.health.dao.PersistenceDataObjectKey;
import com.dnulabs.check.health.processor.CheckHealthProcessor;
import com.dnulabs.check.health.utils.DBUtility;

@WebServlet(urlPatterns = {"/run/schedule", "/run/sig/*"})
public class CheckHealthServlet extends GenericServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2446917600627135098L;
	private static final String APPPIDQUERY = "select data from ApplicationData data where data.appName = :uniqName ";
	private static final String GETLEVELS = "select data from ApplicationEnv data where data.envKey.appId = :uniqId ";

	@SuppressWarnings("unchecked")
	@Override
	public void service(ServletRequest request, ServletResponse response)
			throws ServletException, IOException {
		HashMap<String,Object> strategies = (HashMap<String, Object>) request.getServletContext().getAttribute(Constants.STRATEGIES.toString());
		SessionFactory hbmFactory = DBUtility.getSessionFactory();
		Session hbmSession = hbmFactory.openSession();
		if(request.getParameter("app") != null) {
			String appName = (String) request.getParameter("app");
			singleRun(hbmSession,strategies,appName);
		} else {
			scheduleRun(hbmSession, strategies);
			
		}
		hbmSession.close();
	}
	
	
	@SuppressWarnings("unchecked")
	private void scheduleRun(Session hbmSession, HashMap<String,Object> strategies) {
		PersistenceDataObject data = null;
		Query getAppId = hbmSession.createQuery(APPPIDQUERY);
		Query getLevels = hbmSession.createQuery(GETLEVELS);
		for(Map.Entry<String, Object> entry : strategies.entrySet()) {
			CheckHealthProcessor processor = (CheckHealthProcessor) entry.getValue(); 
			getAppId.setParameter("uniqName", entry.getKey());
			List<ApplicationData> appIdData = getAppId.list();
			if(appIdData.size() > 0) {
				String appId = appIdData.get(0).getAppId();
				getLevels.setParameter("uniqId", appIdData.get(0));
				List<ApplicationEnv> levels = getLevels.list();
				for(ApplicationEnv level : levels) {
					ApplicationEnvKey envKey = new ApplicationEnvKey();
					ApplicationData app = new ApplicationData();
					ApplicationEnv env = new ApplicationEnv();
					PersistenceDataObjectKey dataKey = new PersistenceDataObjectKey();
					app.setAppName(entry.getKey());
					app.setAppId(appId);
					envKey.setAppId(app);
					envKey.setEnvId(level.getEnvKey().getEnvId());
					/*env.setAppId(app);
					env.setEnvId(level.getEnvId());*/
					data = processor.requestProcessor(app,env);
					env.setEnvKey(envKey);
					/*data.setEnvId(env);
					data.setLastUpdatedTimestamp(new Date());*/
					dataKey.setEnvId(env);
					dataKey.setLastUpdatedTimestamp(new Date());
					data.setDataKey(dataKey);
					//data.setAppUniqIdentifier(app);
					processor.tearDown(data);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void singleRun(Session hbmSession, HashMap<String,Object> strategies, String appName) {

		PersistenceDataObject data = null;
		Query getAppId = hbmSession.createQuery(APPPIDQUERY);
		Query getLevels = hbmSession.createQuery(GETLEVELS);
		CheckHealthProcessor processor = (CheckHealthProcessor) strategies.get(appName); 
		getAppId.setParameter("uniqName", appName);
		List<ApplicationData> appIdData = getAppId.list();
		if(appIdData.size() > 0) {
			String appId = appIdData.get(0).getAppId();
			getLevels.setParameter("uniqId", appIdData.get(0));
			List<ApplicationEnv> levels = getLevels.list();
			for(ApplicationEnv level : levels) {
				ApplicationEnvKey envKey = new ApplicationEnvKey();
				ApplicationData app = new ApplicationData();
				ApplicationEnv env = new ApplicationEnv();
				PersistenceDataObjectKey dataKey = new PersistenceDataObjectKey();
				app.setAppName(appName);
				app.setAppId(appId);
				envKey.setAppId(app);
				envKey.setEnvId(level.getEnvKey().getEnvId());
				data = processor.requestProcessor(app,env);
				env.setEnvKey(envKey);
				dataKey.setEnvId(env);
				dataKey.setLastUpdatedTimestamp(new Date());
				data.setDataKey(dataKey);
				processor.tearDown(data);
			}
		}
	}
} 