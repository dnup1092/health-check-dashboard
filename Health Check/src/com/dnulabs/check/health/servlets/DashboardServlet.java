package com.dnulabs.check.health.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

import com.dnulabs.check.health.constants.Constants;

/**
 * Servlet implementation class DashboardServlet
 */
@WebServlet("/dashboard")
public class DashboardServlet extends GenericServlet {
	private static final long serialVersionUID = 1L;
  

	/**
	 * @see Servlet#service(ServletRequest request, ServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		
		HashMap<String,List<String>> categories = (HashMap<String,List<String>>) request.getServletContext().getAttribute(Constants.CATEGORIES.toString());
		request.setAttribute("categories", categories);
		request.getRequestDispatcher("/dashboard.jsp").forward(request,response);
	}

}
