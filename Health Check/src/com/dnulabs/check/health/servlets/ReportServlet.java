package com.dnulabs.check.health.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.dnulabs.check.health.dao.PersistenceDataObject;
import com.dnulabs.check.health.utils.DBUtility;

@WebServlet(urlPatterns={"/summary"})
public class ReportServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4167971903255459800L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		//Date date = new Date();
		
		SessionFactory hbmFactory = DBUtility.getSessionFactory();
		Session hbmSession = hbmFactory.openSession();
		Query query = hbmSession.createQuery("from PersistenceDataObject as envStatus where lastUpdatedDate = current_date");
		//query.setParameter(0,new SimpleDa);
		@SuppressWarnings("unchecked")
		List<PersistenceDataObject> result = query.list();
		hbmSession.close();
		req.setAttribute("report", result);
		req.getRequestDispatcher("/summary.jsp").forward(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		//Date date = new Date();
		String dbQuery = "from PersistenceDataObject as envStatus where lastUpdatedDate = current_date";
		if(null != req.getParameter("appName")) {
			dbQuery = "from PersistenceDataObject as envStatus where lastUpdatedDate = current_date" + " and dataKey.envId.envKey.appId.appName = '" + req.getParameter("appName") + "'";
		}
		SessionFactory hbmFactory = DBUtility.getSessionFactory();
		Session hbmSession = hbmFactory.openSession();
		Query query = hbmSession.createQuery(dbQuery);
		//query.setParameter(0,new SimpleDa);
		@SuppressWarnings("unchecked")
		List<PersistenceDataObject> result = query.list();
		hbmSession.close();
		req.setAttribute("report", result);
		if(null == req.getParameter("appName")) {
			req.getRequestDispatcher("/summary.jsp").forward(req, resp);
		} else {
			req.getRequestDispatcher("/dashboard").forward(req, resp);
		}
	}

}
