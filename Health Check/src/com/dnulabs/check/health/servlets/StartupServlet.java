package com.dnulabs.check.health.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.dnulabs.check.health.constants.Constants;
import com.dnulabs.check.health.dao.ApplicationData;
import com.dnulabs.check.health.dao.ApplicationEnv;
import com.dnulabs.check.health.dao.ApplicationEnvKey;
import com.dnulabs.check.health.utils.DBUtility;

@WebServlet(urlPatterns={"/initialize","/saveConfig"})
public class StartupServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7608716569863446770L;
	private static boolean isInitialized = false;
	private static int semaphore = 0;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(semaphore == 1 && !isInitialized) {
			resp.getWriter().print("<h1>Administrator is setting up the application, please wait . . .</h1>");
			return;
		}
		
		else if(isInitialized) {
			req.getRequestDispatcher("/dashboard").forward(req, resp);
			return;
		}
		
		synchronized(this) {
			if(semaphore == 0) {
				semaphore = 1;
			} else {
				resp.getWriter().print("<h1>Administrator is setting up the application, please wait . . .</h1>");
				return;
			}
			
		}
		
		if(semaphore == 1) {
			resp.sendRedirect(req.getContextPath() + "/startup.html");
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int noOfLevels = Integer.parseInt(request.getParameter(Constants.APP_LEVEL).trim());
		SessionFactory hbmFactory = DBUtility.getSessionFactory();
	    Session hbmSession = hbmFactory.openSession();
	    Transaction hbmTransaction = hbmSession.beginTransaction();
	    ApplicationData app = null;
	    hbmTransaction.begin();
	    
	    String[] appIds = request.getParameterValues("appId");
	    String[] appNames = request.getParameterValues("appName");
	    
	    for(int walker = 0 ; walker < appIds.length; walker++) {
	    	app = new ApplicationData(); 
	    	app.setAppId(appIds[walker]);
	    	app.setAppName(appNames[walker]);
	    	hbmSession.save(app);
	    	  for(int looper = 1; looper<= noOfLevels ; looper++) {
	    		  ApplicationEnv env = new ApplicationEnv();
	    		  ApplicationEnvKey key = new ApplicationEnvKey();
	    		  key.setAppId(app);
	    		  key.setEnvId("L" + looper);
	    		  env.setEnvKey(key);
	    		  /*env.setAppId(app);
	    		  env.setEnvId("L" + looper);*/
	    		  hbmSession.save(env);
	    	  }
	    }
		hbmTransaction.commit();
		isInitialized = true;
		response.sendRedirect(request.getContextPath() + "/dashboard");
	}
	    
}
