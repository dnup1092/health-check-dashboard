package com.dnulabs.check.health.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import com.dnulabs.check.health.dao.ApplicationData;
import com.dnulabs.check.health.dao.ApplicationEnv;
import com.dnulabs.check.health.dao.PersistenceDataObject;
import com.dnulabs.check.health.dao.PersistenceDataObjectKey;

public class DBUtility {

	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory;

	public static boolean tableExists(final Session hbsess,
			final String tableName) throws HibernateException, SQLException {
		Boolean result = false;
		Connection connection = hbsess.getSessionFactory()
				.getSessionFactoryOptions().getServiceRegistry()
				.getService(ConnectionProvider.class).getConnection();
		if (connection != null) {
			ResultSet tables = connection.getMetaData().getTables(null, null,
					tableName, null);
			while (tables.next()) {
				String currentTableName = tables.getString("TABLE_NAME");
				if (currentTableName.equals(tableName)) {
					result = true;
				}
			}
			tables.close();
		}
		return result;
	}

	public static SessionFactory initializeSessionFactory(String driver, String url, String user, String password, String dialect) {
		if (sessionFactory == null) {
			try {
				/*StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

				HashMap<String, String> settings = new HashMap<String, String>();
				settings.put(Environment.DRIVER, driver);
				settings.put(Environment.URL,
						url);
				settings.put(Environment.USER, user);
				settings.put(Environment.PASS, password);
				settings.put(Environment.DIALECT,
						dialect);
				settings.put(Environment.SHOW_SQL,
						"true");
				settings.put(Environment.HBM2DDL_AUTO, "create");
				
				settings.put(Environment.AUTO_CLOSE_SESSION, "true");

				registryBuilder.applySettings(settings);

				registry = registryBuilder.build();

				MetadataSources sources = new MetadataSources(registry);

				Metadata metadata = sources.getMetadataBuilder().build();

				sessionFactory = metadata.getSessionFactoryBuilder().build();*/
				
				
				Properties prop= new Properties();
				prop.setProperty(Environment.DRIVER, driver);
				prop.setProperty(Environment.URL,
						url);
				prop.setProperty(Environment.USER, user);
				prop.setProperty(Environment.PASS, password);
				prop.setProperty(Environment.DIALECT,
						dialect);
				prop.setProperty(Environment.SHOW_SQL,
						"true");
				prop.setProperty(Environment.HBM2DDL_AUTO, "update");
				prop.setProperty(Environment.AUTO_CLOSE_SESSION, "true");
				
				sessionFactory = new Configuration()
			   .addPackage("com.dnulabs.check.health.dao")
					   .addProperties(prop)
					   .addAnnotatedClass(ApplicationData.class)
					   .addAnnotatedClass(ApplicationEnv.class)
					   .addAnnotatedClass(PersistenceDataObjectKey.class)
					   .addAnnotatedClass(PersistenceDataObject.class)
					   .buildSessionFactory();

			} catch (Exception e) {
				e.printStackTrace();
				/*if (registry != null) {
					StandardServiceRegistryBuilder.destroy(registry);
				}*/
			}
		}
		return sessionFactory;
	}
	
	
	public static SessionFactory getSessionFactory() {
		if (sessionFactory != null) {
			return sessionFactory;
		}
		return null; // throw exception here
	}

	public static void shutdown() {
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}