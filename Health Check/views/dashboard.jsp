<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.List"
	import="com.dnulabs.check.health.dao.PersistenceDataObject"
	import="java.util.HashMap" import="java.util.List"
	import="java.util.Map"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Health Check 1.0 | Dashboard</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
<!-- <script type="text/javascript"
	src="http://code.jquery.com/jquery-1.7.1.min.js"></script> -->

<script
	src='https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js'></script>
<script
	src='https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="startup.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src='https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.0.0/ui-bootstrap-tpls.min.js'></script>	

<script type="text/javascript">
function onClick() {
$.post('/TestHealthFramework/run/sig/',{ app : arguments[0]},function(responseText) { 
        
    });
    return false;
}

function onClickLoad() {
	
	var uri = "/TestHealthFramework/" + arguments[0];
	$.post(uri,"",function(responseText) { 
	        
	    });
	    return true;
	}
/* 	
function on() {
    document.getElementById("overlay").style.display = "block";
}

function off() {
    document.getElementById("overlay").style.display = "none";
} */


</script>

<!-- <style>
#overlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0.5);
    z-index: 2;
    cursor: pointer;
}

#text{
    position: absolute;
    top: 50%;
    left: 50%;
    font-size: 50px;
    color: white;
    transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
}
</style> -->


</head>
<body>


	<div class="w3-top">
		<div class="w3-bar w3-border w3-card-4 w3-green">
			<a href="/TestHealthFramework/dashboard"
				class="w3-bar-item w3-button w3-hover-none w3-border-blue w3-bottombar w3-hover-border-blue w3-hover-text-blue ">Home</a>
			<a href="/TestHealthFramework/summary"
				class="w3-bar-item w3-button w3-hover-none w3-border-green w3-bottombar w3-hover-border-blue w3-hover-text-blue">Summary</a>
			<a href="tools.jsp"
				class="w3-bar-item w3-button w3-hover-none w3-border-green w3-bottombar w3-hover-border-blue w3-hover-text-blue w3-mobile"
				style="text-decoration: none;">Tools</a> <a href="downloads.jsp"
				class="w3-bar-item w3-button w3-hover-none w3-border-green w3-bottombar w3-hover-border-blue w3-hover-text-blue w3-mobile"
				style="text-decoration: none;">Downloads</a> <a href="aboutus.jsp"
				class="w3-bar-item w3-button w3-hover-none w3-border-green w3-bottombar w3-hover-border-blue w3-hover-text-blue w3-mobile"
				style="text-decoration: none;">About Us</a>

		</div>
	</div>
	<br>
	<br>
	<div class="w3-container">
		<%
			List<PersistenceDataObject> report = (List<PersistenceDataObject>) request.getAttribute("report");
			if (report != null) {
		%>
		<!-- <div id="overlay" onclick="off()"> -->

			<div class="container">
				<div class="data-table-container">
					<table id="reportId"
						class="table table-hover table-striped table-bordered data-table">
						<thead>
							<th>Level</th>
							<th>Status</th>
						</thead>
						<tbody>
							<%
								for (PersistenceDataObject object : report) {
							%>

							<tr>
								<td><%=object.getDataKey().getEnvId().getEnvKey().getEnvId()%></td>
								<td><%=object.getStatus()%></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
			</div>

		<!-- </div> -->
		<%
			}
			request.setAttribute("report", null);
		%>
		<%
			HashMap<String, List<String>> categories = (HashMap<String, List<String>>) request
					.getAttribute("categories");
			for (Map.Entry<String, List<String>> category : categories.entrySet()) {
		%>
		<div class="w3-panel w3-blue w3-card-4 w3-round-xxlarge">
			<p>
				<%=category.getKey()%>
			</p>
		</div>
		<div class="w3-row-padding">
			<%
				List<String> appNames = category.getValue();
					for (String appName : appNames) {
			%>
			<div class="w3-col s2 w3-center">
				<div class="w3-card-4">
					<a id="submit"
						href="/TestHealthFramework/summary?appName=<%=appName%>"
						title="Summary" style="text-decoration: none;" onclick="on()">
						<header class="w3-container w3-light-grey w3-hover-teal">
							<p style="text-shadow: 1px 1px 0 #444">
								<%=appName%>
							</p>
						</header>
					</a>
					<button class="w3-button w3-block w3-dark-grey w3-hover-teal"
						onclick="return onClick(<%="\'" + appName + "\'"%>)">Checkout</button>
				</div>
			</div>

			<%
			}
		%>
		</div>
		<%
			}
		%>
	</div>

</body>
</html>
