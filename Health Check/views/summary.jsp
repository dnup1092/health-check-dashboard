<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.List"
	import="com.dnulabs.check.health.dao.PersistenceDataObject"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Health Check 1.0 | Summary</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="startup.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src='https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.0.0/ui-bootstrap-tpls.min.js'></script>
<script
	src='https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js'></script>
<script
	src='https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js'></script>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
<script>
	$(document).ready(function() {
		$('#reportId').DataTable({
			rowCallback : function(row, data, index) {
				if (data[2].toUpperCase() == 'PASS') {
					$(row).find('td:eq(0)').css('color', 'green');
					$(row).find('td:eq(1)').css('color', 'green');
					$(row).find('td:eq(2)').css('color', 'green');
				}
				if (data[2].toUpperCase() == 'FAIL') {
					$(row).find('td:eq(0)').css('color', 'red');
					$(row).find('td:eq(1)').css('color', 'red');
					$(row).find('td:eq(2)').css('color', 'red');
				}
			}
		});
	});
	
	function onClickLoad() {
		
		var uri = "/TestHealthFramework/" + arguments[0];
		$.post(uri,"",function(responseText) { 
		        
		    });
		    return false;
		}
</script>
</head>
<body>
<div class="w3-top">
  <div class="w3-bar w3-border w3-card-4 w3-green">
     <a href="/TestHealthFramework/dashboard" class="w3-bar-item w3-button w3-hover-none w3-border-green w3-bottombar w3-hover-border-blue w3-hover-text-blue " style="text-decoration:none;">Home</a>
  <a href="/TestHealthFramework/summary" class="w3-bar-item w3-button w3-hover-none w3-border-blue w3-bottombar w3-hover-border-blue w3-hover-text-blue" style="text-decoration:none;">Summary</a>
 
  <a href="tools.jsp" class="w3-bar-item w3-button w3-hover-none w3-border-green w3-bottombar w3-hover-border-blue w3-hover-text-blue w3-mobile" style="text-decoration:none;">Tools</a>
   <a href="downloads.jsp" class="w3-bar-item w3-button w3-hover-none w3-border-green w3-bottombar w3-hover-border-blue w3-hover-text-blue w3-mobile" style="text-decoration:none;">Downloads</a>
   <a href="aboutus.jsp" class="w3-bar-item w3-button w3-hover-none w3-border-green w3-bottombar w3-hover-border-blue w3-hover-text-blue w3-mobile" style="text-decoration:none;">About Us</a>
  </div>
</div>
<br><br>
	<div class="container">
		<h2>Health Check Summary</h2>
		<div class="data-table-container">
			<table id="reportId"
				class="table table-hover table-striped table-bordered data-table">
				<thead>
					<th>Application</th>
					<th>Level</th>
					<th>Status</th>
				</thead>
				<tbody>
					<%
						List<PersistenceDataObject> report = (List<PersistenceDataObject>) request
								.getAttribute("report");
						for (PersistenceDataObject object : report) {
					%>

					<tr>

						<td><%=object.getDataKey().getEnvId().getEnvKey().getAppId()
						.getAppName()%></td>
						<td><%=object.getDataKey().getEnvId().getEnvKey().getEnvId()%></td>
						<td><%=object.getStatus()%></td>
						<!-- <td class="active">...</td>
				<td class="success">...</td>
				<td class="warning">...</td>
				<td class="danger">...</td>
				<td class="info">...</td> -->
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>

</body>
</html>
